#include "App.h"

#define P0_TIM 53
#define P1_TIM 38
#define P2_TIM 15

/*******************************
数码管显示
***********************************/
u8 rts = 53;//洗衣任务剩余时间

//计算处于运行过程时的剩余时间
void comput_rts(){
	if( wash_status == 0 ) rts = P0_TIM - cnt_ws;
	else if( wash_status == 1 ) rts = P1_TIM - cnt_ws  ;
	else if( wash_status == 2 ) rts = P2_TIM - cnt_ws ;
}

//显示模式
void show_mode(){
	//使用0,1号位显示模式
	NIXIE_display(0,0);
	NIXIE_display(Mode,1);	
}

//显示过程剩余时间,使用0,1位显示剩余时间(单位m)
void show_time_remaining(){
	static unit,ten;
	ten = rts /10;
	unit = rts%10;
  //显示时间
	NIXIE_display(ten,0);
	NIXIE_display(unit,1);	
}



//根据情况显示数码管
void showAndSound(){
	
	if( is_wash_run == 0){
		show_mode();
	}
  else if( is_wash_run == 1 ){
		comput_rts();
		show_time_remaining();	
	} 
	
}


void task_3() _task_ task_nixie {
    
    NIXIE_INIT();
	 

    while(1) {
			  showAndSound();
        os_wait2(K_TMO, 1);
    }
}