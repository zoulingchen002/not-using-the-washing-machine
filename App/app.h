#ifndef __APP_H__
#define __APP_H__

#include "config.h"
#include "STC8G_H_GPIO.h"
#include "STC8G_H_delay.h"
#include "STC8G_H_UART.h"
#include "STC8G_H_NVIC.h"
#include "STC8G_H_Switch.h"

#include "Individual_key.h"//独立按键
#include "buzzer.h"//蜂鸣器
#include "nixie.h"//数码管
#include "steering_engine.h"//进出水口舵机
#include "PCF8563.h"//时钟芯片
#include "roller.h"//洗衣机滚筒马达

extern u8 Mode;//洗衣模式
extern u8 is_stop;//是否暂停
extern u8 is_wash_run;//程序是否已经开始运行
extern u8 is_sound;//蜂鸣器是否发出声响标志位

//当前处于那个洗衣过程,当前过程进行了多久时间戳
extern u8 wash_status;
extern u8 cnt_ws;

//测试
extern u8 rts;

#define task_key     1
#define task_roller  2
#define task_nixie   3
#define task_buzzer  4


//限定范围值的宏
#define get_max(num1,max)  (num1) > (max)? (max):(num1)
#define get_min(num2,min)  (num2) < (min)? (min):(num2)

#endif