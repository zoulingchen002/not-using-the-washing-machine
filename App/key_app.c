#include "App.h"

/******************************
按键任务
*******************************/

u8 count = 0;
//洗衣机模式1,2,3
u8 Mode =1;
//是否暂停,默认0（运行），1（暂停）
u8 is_stop = 0;
//洗衣程序程序是否正在运行0(未运行)，1（运行）
u8 is_wash_run = 0;
//蜂鸣器是否发出声响,0不发出，1发出
u8 is_sound = 0;
//时间戳，用于记录任务的时间每过1s加一
u8 cnt_ws=0;
//洗衣任务是否被创建
u8 w_task_flag = 0;

//洗衣任务通用属性重置
void reset_wash_pro(){
	is_stop = 0;//是否暂停
	is_sound = 0;//蜂鸣器是否发声
	wash_status=0;//阶段
	cnt_ws=0;//计数值
	Buzzer_stop();
}

//按键1按下执行函数
void key1_down(void) {
    //切换模式
    Mode++;
    if( Mode == 4 ) Mode=1;//重置
}

//按键2按下执行函数，启动键，暂停
void key2_down(void) {
    static  cnt = 0;
	
    if( cnt == 0 || is_wash_run == 0 ) {
        //开启程序，或者运行暂停的程序
        if( is_wash_run == 0 ) {
					
					  if(w_task_flag == 1){//洗衣任务自然完成，先删除任务，再执行任务
							os_delete_task(task_roller);
		          w_task_flag = 0;
						}
            //创建任务,标记置1
            os_create_task(task_roller);
					  w_task_flag = 1;
					  reset_wash_pro();//通用属性初始化
					  is_wash_run =1;//执行任务
            
        } else {
            is_stop = 0;
        }
        cnt++;
    } else {
        //暂停
        is_stop = 1;
        cnt = 0;
    }
		
}

//按键3按下执行函数,停止蜂鸣器发出声响
void key3_down(void) {
	is_sound = 0;
	

}

//按键4按下执行函数，重置系统所有状态
void key4_down(void) {
	//只有在创建了洗衣任务才可以去销毁，没创建不需要销毁
	//否则程序可能报错
	if( w_task_flag == 1 ){
		os_delete_task(task_roller);
		w_task_flag = 0;
	}
	Mode = 1;//模式1
	is_wash_run = 0;//未洗衣
	reset_wash_pro();//通用属性初始化

}


void task_1() _task_ task_key {

	  Button_init();


    while(1) {
        button_printf();
        os_wait2(K_TMO, 2);
    }
}