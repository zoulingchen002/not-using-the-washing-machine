#include "App.h"

//进出水口舵机占空比
#define OPENS 1500
#define OFFS  500

/***********************************
洗衣任务
**********************************/

//状态值，记录当前洗衣机执行到程序那个过程0洗衣，1漂洗，2脱水
u8 wash_status = 0;
//马达转速
u8 speed;



//根据模式判断速度
void set_speed() {
    if(Mode == 1 ) speed = 20;
    else if(Mode == 2) speed = 25;
    else if(Mode == 3) speed = 30;
}

//正反转过程,速度和间隔时间单位s
void rollerF0rR(u8 s) {
	roller_foreward(speed);
	os_wait2(K_TMO, s * 200);
	roller_stop();
	os_wait2(K_TMO, s * 50);
  roller_reversal(speed); 
	os_wait2(K_TMO, s * 50);
}


//正转甩干
void rollerF(u8 s) {
  roller_foreward(speed);
	os_wait2(K_TMO, s * 200);
}

//暂停洗衣
void stop_wash(){
	set_angle_in(OFFS);//关闭进水口
	set_angle_out(OFFS);//关闭出水
	roller_stop();
	
}

/***************************************
根据状态修改时钟芯片初始时间值和定时器
过程0 15s  进水3s,洗衣12s
过程1 23s  放水3s,脱水5s ,进水3s,洗衣12s
过程2 15s  放水3s,脱水12s
定时任务，满足条件进入下一个过程状态
***************************************/
void timer_fun() {
    
	  
	  //不处于暂停状态才可以计数
	  if(is_stop == 1 || is_wash_run == 0 ){
			stop_wash();
			return;
		}
		//定时器一个周期1ms
	  cnt_ws++;
	  
    if( wash_status == 0 && cnt_ws == 15 ) {
        //更新状态，清空计数值
        wash_status ++;
        cnt_ws = 0;
    } else if( wash_status == 1 && cnt_ws == 23 ) {
        //更新状态，清空计数值
        wash_status ++;
        cnt_ws = 0;
    } else if( wash_status == 2 && cnt_ws == 15 ) {
        //停止程序
			  cnt_ws = 0;
			  is_wash_run = 0;
			  wash_status = 0;
			  is_sound = 1;//蜂鸣器发出警报
			  
    }	
}

//过程0
void wash_0(){
	//进水or洗衣
	if(cnt_ws <= 3 ){
		set_angle_in(OPENS);//打开进水口
	}else if( cnt_ws > 3 &&  cnt_ws < 15){
		set_angle_in(OFFS);//关闭进水口
		rollerF0rR(3);//洗衣
		
	}else {
		//当前过程结束停止马达运动
		roller_stop();
	}
}

//过程1
void wash_1(){
	//放水，脱水，进水，洗衣
	if(cnt_ws <= 3 ){//放水
		set_angle_out(OPENS);//打开出水口
	}else if( cnt_ws > 3 && cnt_ws <= 8){//脱水
		rollerF(5);
		
	}else if( cnt_ws > 8 && cnt_ws <= 11 ){//进水
		roller_stop();
		set_angle_out(OFFS);//关闭出水
		set_angle_in(OPENS);//打开进水口
		
	}else if( cnt_ws > 11 && cnt_ws < 23 ){//洗衣
		set_angle_in(OFFS);//关闭进水口
		rollerF0rR(3);//洗衣
		
	}else {
		//当前过程结束停止马达运动
		roller_stop();
	}
}

//过程2
void wash_2(){
	//放水3s,脱水12s
	if(cnt_ws <= 3 ){//放水
		set_angle_out(OPENS);//打开出水口
		
	}else if( cnt_ws > 3 && cnt_ws < 15){//脱水
		rollerF(12);	
	}else {
		//当前过程结束,关闭出水口,停止马达运动
		set_angle_out(OFFS);
		roller_stop();
	}
}


/*********************************
过程0 15s  进水3s,洗衣12s
过程1 23s  放水3s,脱水5s ,进水3s,洗衣12s
过程2 15s  放水3s,脱水12s
********************************/
void washing_machine() {
	  //暂停状态不进行任何操作0正常工作，1暂停
	  if(is_stop == 1) return;
	  if(is_wash_run == 0) return;
    //过程0
    if( wash_status == 0 && is_stop == 0 ) {
			wash_0();
		//过程1
    } else if( wash_status == 1 && is_stop == 0  ) {
			wash_1();

		//过程2
    } else if( wash_status == 2 && is_stop == 0  ) {
			wash_2();
    }
}



void task_2() _task_ task_roller {
	  
    roller_init();
	  steering_init();
	  PCF8563_init();
	  //设置马达速度
    set_speed();
	

    //设置定时器
    set_timer(HZ64,64);//每个时钟周期1s
    //开启定时器
    isEnable_timer(ENABLE);
	

    while(1) {
			
			  washing_machine();
        os_wait2(K_TMO, 10);
    }
}