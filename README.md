# 没用洗衣机

#### 介绍
基于RTX51系统 的 模拟洗衣机（这玩意儿算个小玩具，不能真用来洗衣服），项目用c语言写的，可以用来锻炼自己对51操作系统的熟悉度，
本项目主要用到：
1. stc8h8k6u芯片（48引脚）
2. 舵机
3. 马达 和 L298N模块
4. PCF8563时钟芯片
5. 蜂鸣器
6. 数码管
7. 独立按键

#### 软件架构
软件架构说明
采用了51操作系统，实现多任务切换功能

#### 安装教程

1.  使用stcai-isp 烧录软件烧录 .hex文件


#### 使用说明

1.  代码中不同模块只需要修改成自己电路板上引脚应该就可以使用
（仅限stc8h8k64u芯片，我用了他们家的库函数）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
