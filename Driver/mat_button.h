#ifndef  __MAT_BUTTON_H__
#define  __MAT_BUTTON_H__

#include "config.h"

//行
#define ROW1 P34
#define ROW2 P35
#define ROW3 P40
#define ROW4 P41
//列
#define COL1 P03
#define COL2 P06
#define COL3 P07
#define COL4 P17

//按键初始化
void BUTTON_GPIO_CONFIG(void);
//判断按键是否按下
void key_fun();
//按键按下执行
//extern void fun_d(u8 row,u8 col);
//按键抬起执行
//extern void fun_u(u8 row,u8 col);
#endif