#include "nixie.h"
#include "STC8G_H_GPIO.h"

#define GET_BIT_VAL(byte,pos)  ( (byte) & (1<<(pos) ) )

//0 	1	 2	-> 9	(索引012...9)
//0  1. 2. -> 9.	(索引10,11,12....19)
//. -						(索引20,21)
// AbCdEFHJLPqU		(索引22,23,24....33)
u8 xdata NIXIE_TABLE[] =
{

    0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,

    0x40,0x79,0x24,0x30,0x19,0x12,0x02,0x78,0x00,0x10,

    0x7F, 0xBF,

    0x88,0x83,0xC6,0xA1,0x86,0x8E,0x89,0xF1,0xC7,0x8C,0x98,0xC1
};

void NIXIE_INIT(void) {
    GPIO_InitTypeDef	GPIO_InitStructure;		//结构定义
    GPIO_InitStructure.Pin  = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_PullUp;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P4, &GPIO_InitStructure);//初始化
}


/*****位移运算*****/
static void NIXIE_out(u8 dat) {
    int i;
    for(i=7; i>=0 ; i--) {
        // NIXIE_DI = GET_BIT_VAL( dat,i) ;
        NIXIE_DI = GET_BIT_VAL(dat,i) >> (i) ;
        //赋值后移位
        NIXIE_SCK = 0;
        NOP_TIME();
        NIXIE_SCK = 1;
        NOP_TIME();
    }
}

//根据16进制数显示数码管，前者显示数字，后者决定那个数码管亮
void NIXIE_Show(u8 a_dat, u8 b_idx) {

    // 先发字母位 (控制显示的内容)	// 0点亮
    // 8bit，先发出去的会作为高位
    NIXIE_out(a_dat);


    // 再发数字位 （控制显示哪几个） // 只要不是0，就是高电平
    // 8bit，先发出去的会作为高位
    NIXIE_out(b_idx);

    //锁存操作
    NIXIE_RCK = 0;
    NOP_TIME();
    NIXIE_RCK = 1;
    NOP_TIME();



}


void NIXIE_display(u8 num, u8 pos){
	// 根据num从表中取出对应字节 (显示的内容)
	u8 a_num = NIXIE_TABLE[num];
	// 显示在什么位置 0000 0001
	u8 d_idx = 1 << pos;
	
	NIXIE_Show(a_num, d_idx);
}


void NIXIE_clear() {
    // 先发字母位 (控制显示的内容)	// 0点亮
    // 8bit，先发出去的会作为高位
    NIXIE_out(0xff);

    // 再发数字位 （控制显示哪几个） // 只要不是0，就是高电平
    // 8bit，先发出去的会作为高位
    NIXIE_out(0xff);

    //锁存操作
    NIXIE_RCK = 0;
    NOP_TIME();
    NIXIE_RCK = 1;
    NOP_TIME();
}