#ifndef  __ROLLER_H__
#define  __ROLLER_H__

#include "config.h"

//初始化
void roller_init(void);
//停止转动
void roller_stop(void);
//正转
void roller_foreward(u8);
//反转
void roller_reversal(u8);

#endif