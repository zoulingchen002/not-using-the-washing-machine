#include "buzzer.h"
#include "STC8G_H_GPIO.h"
#include "STC8H_PWM.h"
#include "STC8G_H_NVIC.h"
#include "STC8G_H_Switch.h"
#include "STC8G_H_Delay.h"



static u16 hz[] = {523, 587, 659, 698, 784, 880, 988, 1047};

void buzzer_init(void) {
    GPIO_InitTypeDef initTypeDef;
    initTypeDef.Mode = GPIO_PullUp;
    initTypeDef.Pin = GPIO_Pin_0;
    GPIO_Inilize(GPIO_P0,&initTypeDef);
}

/*****改变频率******/
void	buzzer_set_hz(u16 hz_val)
{

    PWMx_InitDefine		PWMx_InitStructure;

    PWMx_InitStructure.PWM_Mode    =	CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    = (u16)((MAIN_Fosc / hz_val)*0.5);	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect   = ENO5P;	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM5, &PWMx_InitStructure);			//初始化PWM,  PWMA,PWMB

    PWMx_InitStructure.PWM_Period   = (MAIN_Fosc / hz_val) -1;//周期时间,   0~65535
    PWMx_InitStructure.PWM_DeadTime = 0;					//死区发生器设置, 0~255
    PWMx_InitStructure.PWM_MainOutEnable= ENABLE;			//主输出使能, ENABLE,DISABLE
    PWMx_InitStructure.PWM_CEN_Enable   = ENABLE;			//使能计数器, ENABLE,DISABLE
    PWM_Configuration(PWMB, &PWMx_InitStructure);			//初始化PWM通用寄存器,  PWMA,PWMB

    PWM5_SW(PWM5_SW_P00);

    NVIC_PWM_Init(PWMA,DISABLE,Priority_0);
}

void Buzzer_stop(){
	PWMx_Duty dutyB;
	dutyB.PWM5_Duty = 0;
	UpdatePwm(PWM5,&dutyB);
}


/******放歌*****/
void buzzer_beep(u16 hz_val_index) {
    u16 hz_val = hz[hz_val_index - 1];
	  buzzer_set_hz(hz_val);
}

//警报声
void buzzer_alarm(){
	
	buzzer_beep(7);
				
}



