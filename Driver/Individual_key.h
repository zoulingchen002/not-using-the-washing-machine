#ifndef __INDIVIDUAL_KEY_H__
#define __INDIVIDUAL_KEY_H__

#include "config.h"

#define KEY1  P51
#define KEY2  P52
#define KEY3  P53
#define KEY4  P54

void	Button_init(void);

void  button_printf();

//按键1按下执行函数
extern void key1_down(void);
//按键2按下执行函数
extern void key2_down(void);
//按键3按下执行函数
extern void key3_down(void);
//按键4按下执行函数
extern void key4_down(void);

#endif