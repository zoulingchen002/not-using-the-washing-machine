#ifndef  __STEERING_ENGINE_H__
#define  __STEERING_ENGINE_H__

#include "config.h"
//初始化
void steering_init(void);
//舵机角度调整进水口
void set_angle_in(int16 angle);
//舵机角度调整出水口
void set_angle_out(int16 angle);

#endif