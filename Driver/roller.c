#include "roller.h"
#include "STC8G_H_GPIO.h"
#include "STC8G_H_Switch.h"
#include "STC8G_H_NVIC.h"
#include "STC8H_PWM.h"

#define  FREQ  1000

#define  PREIOD  ((MAIN_Fosc/FREQ) -1)

char roller = 50;

//初始化
void roller_init(){
	P1_MODE_OUT_PP(GPIO_Pin_0);
	P1_MODE_OUT_PP(GPIO_Pin_1);
}

/******pwm初始化函数******/
void	roller_PWM_config()
{   
	
	  u16 m_duty = PREIOD * roller /100;
	
	  //通道1p/1n是否选择
		u8 is_run = (roller != 50) ? (ENO1P | ENO1N) :0;
		//是否开始使能
	  u8 is_main_enable = (is_run != 0) ? ENABLE:DISABLE ;
	  
    PWMx_InitDefine		PWMx_InitStructure;
	
	  printf("roller=%d\n",(int)roller);
	  printf("is_run=%d\n",(int)is_run);

	  // 配置PWM1 洗衣机滚筒马达
    PWMx_InitStructure.PWM_Mode    =	CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    =  m_duty;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect   = is_run;	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM1, &PWMx_InitStructure);			//初始化PWM,  PWMA,PWMB

    PWMx_InitStructure.PWM_Period   = PREIOD;					//周期时间,   0~65535
    PWMx_InitStructure.PWM_DeadTime = 10;					//死区发生器设置, 0~255
    PWMx_InitStructure.PWM_MainOutEnable= is_main_enable;			//主输出使能, ENABLE,DISABLE
    PWMx_InitStructure.PWM_CEN_Enable   = is_main_enable;			//使能计数器, ENABLE,DISABLE
    PWM_Configuration(PWMA, &PWMx_InitStructure);			//初始化PWM通用寄存器,  PWMA,PWMB


    PWM1_SW(PWM1_SW_P10_P11);			//PWM1_SW_P10_P11,PWM1_SW_P20_P21,PWM1_SW_P60_P61

    NVIC_PWM_Init(PWMA,DISABLE,Priority_0);
		
		
}

//档位转占空比,参数1档位，参数2正转还是反转
char tp2duty(u8 tp,char aOrB){
	return (aOrB > 0)?  50 - tp : 50 + tp ;
}

//停止转动
void roller_stop(){
	roller = 50;
	roller_PWM_config();
}

//马达正转
void roller_foreward(u8 tp){
	roller = tp2duty(tp,1);
	roller_PWM_config();
} 

//马达反转
void roller_reversal(u8 tp){
	roller = tp2duty(tp,-1);
	roller_PWM_config();
} 

