#include "PCF8563.h"
#include "STC8G_H_GPIO.h"
#include "STC8G_H_I2C.h"
#include "STC8G_H_NVIC.h"
#include "STC8G_H_Switch.h"
#include "STC8G_H_Exti.h"

// 十位取出左移4位 + 个位 (得到BCD数)
#define WRITE_BCD(val) 	(( (val) / 10) << 4) + ( (val) % 10)

// 将高4位乘以10 + 低四位 (得到10进制数) pos : 高四位保留位数 例子，0x03,保留高四位的4位和5位
#define READ_BCD(val,pos) 	( ( (val) >> 4 ) & (pos)) * 10 +  ( (val) & 0x0F)

u8 dat[7];

void I2C_GPIO_config() {

    GPIO_InitTypeDef	GPIO_InitStructure;		//结构定义
    GPIO_InitStructure.Pin  = GPIO_Pin_3 | GPIO_Pin_2;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_OUT_OD;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P3, &GPIO_InitStructure);//初始化

    //中断初始化
    GPIO_InitStructure.Pin  = GPIO_Pin_7;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_PullUp;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P3, &GPIO_InitStructure);//初始化

}

/****************  I2C初始化函数 *****************/
void	I2C_config(void)
{
    I2C_InitTypeDef		I2C_InitStructure;

    I2C_InitStructure.I2C_Mode      = I2C_Mode_Master;	//主从选择   I2C_Mode_Master, I2C_Mode_Slave
    I2C_InitStructure.I2C_Enable    = ENABLE;			//I2C功能使能,   ENABLE, DISABLE
    I2C_InitStructure.I2C_MS_WDTA   = DISABLE;			//主机使能自动发送,  ENABLE, DISABLE
    I2C_InitStructure.I2C_Speed     = 13;				//总线速度=Fosc/2/(Speed*2+4),
    // 400k, 24M => 13
    // 100k, 24M => 58
    I2C_Init(&I2C_InitStructure);
    NVIC_I2C_Init(I2C_Mode_Master,DISABLE,Priority_0);	//主从模式, I2C_Mode_Master, I2C_Mode_Slave; 中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3

    I2C_SW(I2C_P33_P32);					//I2C_P14_P15,I2C_P24_P25,I2C_P33_P32
}

/***外部中断3初始化，因为时钟芯片是给核心板中断3发送下降沿信号**/
void Exit_3_config()
{
    EXTI_InitTypeDef	Exti_InitStructure;							//结构定义
    Exti_InitStructure.EXTI_Mode      = EXT_MODE_Fall;//中断模式,   EXT_MODE_RiseFall,EXT_MODE_Fall
    Ext_Inilize(EXT_INT3,&Exti_InitStructure);				//初始化
    NVIC_INT3_Init(ENABLE,Priority_0);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
}

void PCF8563_init() {

	  Exit_3_config();
    I2C_GPIO_config();
    I2C_config();
}

//通过I2C写入时钟
void write_PCF8563_time(Clock_t cl) {

    //当前世纪 -0 下个世纪 - 1
    u8 c =  cl.century == CENTURY ? 0: 1 ;

    //秒，把结构体内十进制数据转换成BCD格式数据
    dat[0]  =   WRITE_BCD(cl.seconds);
    //分
    dat[1]  =   WRITE_BCD(cl.minutes);
    //时
    dat[2]  =   WRITE_BCD(cl.hours);
    //天
    dat[3]  =   WRITE_BCD(cl.day);
    //周
    dat[4]  =   cl.week ;
    //月,世纪
    dat[5]  =   WRITE_BCD(cl.month) + (c<<7);
    //年,只存年的十位及以下
    dat[6]  =   WRITE_BCD(cl.year - cl.century );

    //void I2C_ReadNbyte(u8 dev_addr, u8 mem_addr, u8 *p, u8 number)
    I2C_WriteNbyte(PCF8563_ADDR,PCF8563_REF,dat,7);

}

//通过I2C读取时钟
void read_PCF8563_time(Clock_t* cl) {

    //void I2C_ReadNbyte(u8 dev_addr, u8 mem_addr, u8 *p, u8 number)
    I2C_ReadNbyte(PCF8563_ADDR,PCF8563_REF,dat,7);
    //秒 把BCD格式数据 结转换成 构体内十进制数据
    cl->seconds  =   READ_BCD(dat[0],0x07);
    //分
    cl->minutes  =   READ_BCD(dat[1],0x07);
    //时
    cl->hours    =   READ_BCD(dat[2],0x03);
    //天
    cl->day      =   READ_BCD(dat[3],0x03);
    //周
    cl->week     =   dat[4] & 0x07 ;
    //月
    cl->month    =   READ_BCD(dat[5],0x01);
    //世纪,0 - 当前世纪  1 -下个世纪
    cl->century  =   (dat[5] >>7) ==0 ? CENTURY: CENTURY +100   ;
    //年
    cl->year     =   READ_BCD(dat[6],0x0f) + cl->century ;
}

//设置并开启闹钟
void set_alarm(Alarm al) {
    u8 arm[4];

    //------------设置闹钟----------------
    //分钟，最高位可开启使能，0开启，芯片默认1(外部输入为-1时软件实现关闭)
    arm[0] = (al.minutes == -1 )? 0x80 : WRITE_BCD(al.minutes);
    //小时，最高位可开启使能，0开启，芯片默认1(外部输入为-1时软件实现关闭)
    arm[1] = (al.hours == -1) ? 0x80 : WRITE_BCD(al.hours);
    //天，最高位可开启使能，0开启，芯片默认1(外部输入为-1时软件实现关闭)
    arm[2] = (al.day == -1) ? 0x80 : WRITE_BCD(al.day);
    //周，最高位可开启使能，0开启，芯片默认1(外部输入为-1时软件实现关闭)
    arm[3] = (al.week == -1) ? 0x80 : al.week;
    I2C_WriteNbyte(PCF8563_ADDR,PCF8563_ALARM_REF,arm,4);

}

//开启闹钟
void isEnable_alarm(u8 enable) {
    u8 cs2;
    //开启闹钟使能，先读出数据再写入
    I2C_ReadNbyte(PCF8563_ADDR,PCF8563_CONTR_REF,&cs2,1);
    cs2 = (enable == ENABLE)?(cs2|0x02):(cs2 & ~0x02); //让控制器使能AIE位置1/0，开启or关闭闹钟
    cs2 &= ~0x08;//清除中断
    I2C_WriteNbyte(PCF8563_ADDR,PCF8563_CONTR_REF,&cs2,1);
}

//设置定时器
void set_timer(timeFreq hz,u8 countdown) {
    u8 cs2;//计数值，下面每次赋值都覆盖上一个数据
    // 启用Timer频率，TE设置为1. 设置运行频率0x01 -> 64Hz
    cs2 = hz + 0x80;
    I2C_WriteNbyte(PCF8563_ADDR,PCF8563_TIMER_col,&cs2,1);
    // 设置计数值(每个周期的计数值)
    cs2 = countdown;
    I2C_WriteNbyte(PCF8563_ADDR, PCF8563_TIMER_value, &cs2, 1);
}

//开启定时器
void isEnable_timer(u8 enable) {
    u8 cs2;
    //开启定时器使能，先读出数据再写入
    I2C_ReadNbyte(PCF8563_ADDR,PCF8563_CONTR_REF,&cs2,1);
    cs2 = (enable == ENABLE)?(cs2|0x01):(cs2 & ~0x01);//让控制器使能TIE位置位置1/0，开启or关闭定时器
    cs2 &= ~0x04;//清除中断
    I2C_WriteNbyte(PCF8563_ADDR,PCF8563_CONTR_REF,&cs2,1);
}

//外部中断3,需要在STC8G_H_Exti.c文件的中断3里调用这个函数
void ext_int3_call() {
    u8 cs2;

    //读取闹钟状态，定时器状态
    I2C_ReadNbyte(PCF8563_ADDR,PCF8563_CONTR_REF,&cs2,1);

#if USE_ALARM
    //判断闹钟触发
    if((cs2 & 0x08) && (cs2 & 0x02)) {
        alarm_fun();
    }
#endif
		

#if USE_TIMER
    //判断定时器触发
    if((cs2 & 0x04) && (cs2 & 0x01)) {
        timer_fun();
    }
#endif
		//只要触发，这两个中断标记都清0
		cs2 &= ~0x08;//清除中断
		cs2 &= ~0x04;//清除中断

    I2C_WriteNbyte(PCF8563_ADDR,PCF8563_CONTR_REF,&cs2,1);
}









