#ifndef	 __Nixie_H__
#define __Nixie_H__

#include "config.h"

#define	NIXIE_DI	P44	// ��������
#define	NIXIE_SCK	P42	// ��λ�Ĵ���
#define	NIXIE_RCK	P43	// ����Ĵ���

#define NOP_TIME()  NOP2()


void NIXIE_INIT(void);

void NIXIE_Show(u8 a_dat, u8 b_idx);

void NIXIE_clear();

void NIXIE_display(u8 num, u8 pos);

#endif