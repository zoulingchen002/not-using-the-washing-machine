#include "Individual_key.h"
#include "STC8G_H_GPIO.h"

//低四位代表四个按键状态标志位 分别是按键4，按键3，按键2，按键1
u8 KEY_status = 0x0f ;


/******************** IO配置函数 **************************/
static void	Button_GPIO_config(void)
{
	GPIO_InitTypeDef	GPIO_InitStructure;		//结构定义
	GPIO_InitStructure.Pin  = GPIO_Pin_1 |GPIO_Pin_2 |GPIO_Pin_3 |GPIO_Pin_4;		//指定要初始化的IO,
	GPIO_InitStructure.Mode = GPIO_PullUp;		//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
	GPIO_Inilize(GPIO_P5,&GPIO_InitStructure);	//初始化
	
}


void	Button_init(){

	Button_GPIO_config();
	
}

//比较按键状态，pos代表是按键数，1-4,high_low代表高低电平
// ( KEY_status & ( 1<< (pos-1) ) ) >> (pos-1) 先置0其余位，然后把计数位移动到第0位
#define compare_key(key,pos,high_low)   ( ( ( KEY_status & ( 1<< (pos-1) ) ) >> (pos-1) )  != (key) ) && (key) == (high_low)

void  button_printf(){
	
	  //按键1按下了
	  if( compare_key( KEY1,1,0) ){
			//printf("按键1优雅的按下了");
			key1_down();
			KEY_status ^= 1; //取反
		}else if( compare_key( KEY1,1,1) ){
			//printf("按键1优雅的抬起了");
			
			KEY_status ^= 1;//取反
		}
		
		//按键2按下了
	  if( compare_key( KEY2,2,0) ){
			//printf("按键2优雅的按下了");
			key2_down();
			KEY_status ^= (1 << 1);//取反
		}else if( compare_key( KEY2,2,1) ){
			//printf("按键2优雅的抬起了");
			
			KEY_status ^= (1 << 1);//取反
		}
		
		//按键3按下了
	  if( compare_key( KEY3,3,0) ){
			//printf("按键3优雅的按下了");
			key3_down();
			KEY_status ^= (1 << 2);//取反
		}else if( compare_key( KEY3,3,1) ){
			//printf("按键3优雅的抬起了");
			
			KEY_status ^= (1 << 2);//取反
		}
		
		//按键4按下了
	  if( compare_key( KEY4,4,0) ){
			//printf("按键4优雅的按下了");
			key4_down();
			KEY_status ^= (1 << 3);//取反
		}else if( compare_key( KEY4,4,1) ){
			//printf("按键4优雅的抬起了");
			
			KEY_status ^= (1 << 3);//取反
		}
	
}