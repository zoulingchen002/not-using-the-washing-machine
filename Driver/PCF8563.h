#ifndef __PCF8563_H__
#define __PCF8563_H__

#include "config.h"

// 设备地址
#define		PCF8563_ADDR  0xa2
// 时钟寄存器地址
#define		PCF8563_REF  0x02
// 控制寄存器地址(用于启用闹钟或者定时器)
#define		PCF8563_CONTR_REF  0x01
// 闹钟寄存器地址
#define		PCF8563_ALARM_REF  0x09 
// 定时器控制寄存器地址,开启和设置定时器频率
#define		PCF8563_TIMER_col  0x0E
// 定时器周期设置寄存器地址,设置定时器的计数值，结合频率最终决定定时器执行时间
#define		PCF8563_TIMER_value  0x0F
//当前世纪
#define   CENTURY   2000

//手动开关，定时器和 闹钟实现函数
#define USE_ALARM		0
#define USE_TIMER	  1

typedef enum {
	
	HZ4096 = 0x00,
	HZ64 	 = 0x01,
	HZ1 	 = 0x02,
	HZ1_64 = 0x03,

}timeFreq;//frequence

typedef struct{

	u16 year;
	u8  month;
	u8  week;
	u8  day;
	u8  hours;
	u8  minutes;
	u8  seconds;
	u16  century;
}Clock_t;

typedef struct{
	char week;
	char day;
	char hours;
	char minutes;
}Alarm;

//初始化
void PCF8563_init();
//读PCF8563时间
void read_PCF8563_time(Clock_t*);
//写PCF8563时间
void write_PCF8563_time(Clock_t);
//设置闹钟
void set_alarm(Alarm);
//开启闹钟
void isEnable_alarm(char isEnable);
//设置定时器
void set_timer(timeFreq hz,u8 countdown);
//开启定时器
void isEnable_timer(char isEnable);

#if USE_ALARM
//闹钟执行任务，实现请先手动把 USE_ALARM置1
extern void alarm_fun();
#endif

#if USE_TIMER
//定时器执行任务，实现请先手动把 USE_TIMER置1
extern void timer_fun();
#endif

#endif