#include "mat_button.h"
#include "STC8G_H_GPIO.h"
#include "STC8G_H_UART.h"

//从4行4列开始，每4位代表一行，到1行1列结束，降序排列
u16 k_status = 0xffff;

void BUTTON_GPIO_CONFIG(){

    P0_MODE_IO_PU(GPIO_Pin_3|GPIO_Pin_6|GPIO_Pin_7);
    P1_MODE_IO_PU(GPIO_Pin_7);
    P3_MODE_IO_PU(GPIO_Pin_4|GPIO_Pin_5);
    P4_MODE_IO_PU(GPIO_Pin_0|GPIO_Pin_1);

}

//初始化某一行,置0
void low_row(u8 rowId){
	ROW1 = (rowId != 0);
	ROW2 = (rowId != 1);
	ROW3 = (rowId != 2);
	ROW4 = (rowId != 3);
}

//判断读那一列
u8 read_col(u8 col){
	if(col == 0) return COL1;
	if(col == 1) return COL2;
	if(col == 2) return COL3;
	if(col == 3) return COL4;
	return FAIL;
}

//状态位移数
#define pos(row,col)  ((row * 4) + col)

//判断按键是否按下
void key_fun(){
	u8 row;
	u8 col;
	u8 key;//当前按键的高低电平 
	for(row = 0;row < 4;row++){
		low_row(row);//设置行状态
		for(col = 0;col <4 ;col++){
			key = read_col(col);//读列状态
			if(key == -1) continue;
			//判断当前列状态 和 上一次的状态（把u16记录那一位值移到第0位，然后把其它位置1）是否不同
			if( key != (( k_status >> pos(row,col))&1) ){
				//printf("%d行%d列值%d\n",(int)(row+1),(int)(col+1),(int)pos(row,col));
				if(key){
//					printf("%d行%d列按下\n",(int)(row+1),(int)(col+1));
//					fun_d(row,col);
				}else{
//					printf("%d行%d列抬起\n",(int)(row+1),(int)(col+1));
//					fun_u(row,col);
				}
				k_status ^= 1<< pos(row,col); //对设置的位置取反,与0异或不变，与1异或取反
			}
			
		}
	}
}
