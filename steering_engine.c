#include "steering_engine.h"
#include "STC8G_H_GPIO.h"
#include "STC8G_H_NVIC.h"
#include "STC8H_PWM.h"
#include "STC8G_H_Switch.h"

// 分频系数：可以是1~65535中的任意值
#define Prescaler 10
#define PREQ  50
#define PERIOD  (MAIN_Fosc/(PREQ*Prescaler))

PWMx_Duty PWMB_Duty;

static void steering_gpio_config() {

    P0_MODE_OUT_PP(GPIO_Pin_1);//初始化p0.1引脚为推挽输出，进水口
	  P0_MODE_OUT_PP(GPIO_Pin_2);//初始化p0.2引脚为推挽输出，出水口
}

static void	PWM_config(void)
{
    PWMx_InitDefine		PWMx_InitStructure;

    PWMx_InitStructure.PWM_Mode    =	CCMRn_PWM_MODE1;	//模式,（模式2，占空比越高，高电平比例越低，模式1（这个和行业标准契合）恰恰相反）		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    = 500;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect   = ENO6P;				//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM6, &PWMx_InitStructure);		//初始化PWM,  PWMA,PWMB

    PWMx_InitStructure.PWM_Mode    =	CCMRn_PWM_MODE1;	//模式,（模式2，占空比越高，高电平比例越低，模式1（这个和行业标准契合）恰恰相反）		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    = 500;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect   = ENO7P;				//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM7, &PWMx_InitStructure);		//初始化PWM,  PWMA,PWMB

    PWMx_InitStructure.PWM_Period   = PERIOD -1;					//周期时间,   0~65535
    PWMx_InitStructure.PWM_DeadTime = 0;					//死区发生器设置, 0~255
    PWMx_InitStructure.PWM_MainOutEnable= ENABLE;			//主输出使能, ENABLE,DISABLE
    PWMx_InitStructure.PWM_CEN_Enable   = ENABLE;			//使能计数器, ENABLE,DISABLE
    PWM_Configuration(PWMB, &PWMx_InitStructure);			//初始化PWM通用寄存器,  PWMA,PWMB

    // 设置预分频系数
    PWMB_Prescaler(Prescaler - 1);

    PWM6_SW(PWM6_SW_P01);					//PWM6_SW_P21,PWM6_SW_P54,PWM6_SW_P01,PWM6_SW_P75
		PWM7_SW(PWM7_SW_P02);					

    NVIC_PWM_Init(PWMB,DISABLE,Priority_0);

}

void steering_init() {
    steering_gpio_config();
    PWM_config();

}

//舵机驱动进水口
void set_angle_in(int16 value) {
    PWMB_Duty.PWM6_Duty=PERIOD * value/20000 ;
    UpdatePwm(PWM6,&PWMB_Duty);
	
}

//舵机驱动出水口
void set_angle_out(int16 value){
	  PWMB_Duty.PWM7_Duty=PERIOD * value/20000 ;
	  UpdatePwm(PWM7,&PWMB_Duty);
}
